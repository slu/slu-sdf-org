.PHONY: show-help serve clean
.nop_target:

SHELL := /bin/bash

show-help: # display this help (default target)
	@echo "Use one of the following targets:"
	@echo
	@$(MAKE) --print-data-base .nop_target \
		| awk '/^[a-z-]+:/{print "^" $$1}' \
		| grep -Ef- Makefile \
		| awk '!seen[$$1]++' \
		| sed -e 's/:.*#/:/' \
		| column -ts: \
		| sed 's/^/    /'

build: # build all sites
	@figlet Bulding all...
	@for site in slu-sdf-org sdfeu-org; do \
	    echo "========== Building site $$site =========="; \
	    $(MAKE) SITE="$$site" --makefile=Makefile.build; \
	    echo "========== Done building $$site =========="; \
	done

serve: # serve the build content on localhost port 8000
	python -m http.server -d build

publish: # publish latest version from git to eu and us hosts
	@figlet Publishing...
	@echo "========== Retrieving latest =========="
	git clone "$$(git config --get remote.origin.url)" publish
	@echo "========== Done retrieving =========="
	$(MAKE) BUILD_ID=1 --directory publish build
	@echo "========== Uploading to servers =========="
	$(MAKE) --directory publish --makefile=Makefile.publish
	@echo "========== Done uploading =========="

clean: # delete generated files
	@figlet Deleting files...
	rm -fR publish
	$(MAKE) --makefile=Makefile.build clean
	$(MAKE) --makefile=Makefile.publish clean
