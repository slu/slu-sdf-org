# Søren's Corner

The contents that is published to my personal old-school website:
https://slu.sdf.org/

Also includes scripts to support assembling and publishing contents.

## Structure

The project is layed out in three directories and three Makefiles:

``` text
html/
  index.html
  ...
inc/
  footer.html
  header.html
  ...
scripts/
  build.pl
Makefile
Makefile.build
Makefile.publish
```

The `html` directory contains the main conents, i.e., HTML files,
images, and other files. The HTML files are not _complete_, as they
are missing, e.g, headers and footers. The headers, footers, and other
content is added to the HTML file during the build. The added content
is stored in files in the `inc` directory. To build the project the
`build.pl` script in the `build` directory is called on each HTML file
and stores the result in a `build` directory. Non-HTML files are
copied to the `build` directory without processing.


``` text
build/
  sdfeu-org/
    index.html
    ...
  slu-sdf-org/
    index.html
    ...
```
