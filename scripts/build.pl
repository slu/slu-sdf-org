#!/usr/bin/env perl

use strict;
use warnings;
use v5.36;
use open qw(:std :utf8);

use File::Basename;
use POSIX qw(strftime);

binmode(STDOUT, ":utf8");

die "USAGE: $0 <filename>" if($#ARGV != 0);
my $script_dir = dirname(__FILE__);
my $inc_dir = "$script_dir/../inc";

# <!--%include "<filename>" -->
# Replace with contents of <filename>. If <filename> doesn't exist, just remove.
#
# <!--%include "<filename>" --always -->
# As above, but fail if <filename> doesn't exist.

my $site = $ENV{'SITE'} || 'slu-sdf-org';
my $other_site_url = ($site eq 'slu-sdf-org' ? 'https://sdfeu.org/~soren/' : 'https://slu.sdf.org/');
my @countries = ('Seattle (USA)', 'Germany (Europe)');
my $country_index = ($site eq 'slu-sdf-org' ? 0 : 1);
my %vars = (site => $site,
            other_site_url => $other_site_url,
            country => $countries[$country_index],
            other_country => $countries[1-$country_index]);

sub note(@msg) { print STDERR @msg,"\n"; }

sub expand($str) {
    $str =~ s/\$(\w+)/
      exists $vars{$1} ? $vars{$1} : '???'
      /eg;
    return $str;
}

sub get_git_time($filename) {
    return qx(git log -1 --format="%at" -- "$filename")
}

my $year = 2023;
my $cur_year = 1900 + (localtime)[5];
$vars{year} = $cur_year > $year ? "$year-$cur_year" : "$year";

my $filename = "";

while(<>) {
    if ($filename ne $ARGV) {
        $filename = $ARGV;
        my $unix_time = exists $ENV{'BUILD_ID'} ? get_git_time($filename) : (stat ($filename))[9];
        $vars{modified_date} = strftime('%A, %b %-e, %Y', localtime($unix_time));

        for my $var (sort keys %vars) {
            note("$var = $vars{$var}");
        }
    }

    if(/^<!--%([a-z]+) (.*) -->/) {
        if ($1 eq "include") {
            my @args = split(/\s/, expand($2));
            my $filename = shift @args;
            if ($filename =~ /"(.+)"/) {
                my $fullpath = "$inc_dir/$1";
                if (-e $fullpath) {
                    my $contents = do {
                        open my $fh, '<:encoding(UTF-8)', $fullpath or die;
                        local $/;
                        <$fh>;
                    };
                    print expand($contents);
                } else {
                    note("Cannot find $fullpath")
                }
            }
        } else {
            note("Unknown directive: $1");
            exit 1;
        }
    } else {
        print expand($_);
    }
}
