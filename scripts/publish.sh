#!/usr/bin/env bash

tmpdir=$(mktemp -d); trap 'rm -rf -- "$tmpdir"' EXIT

git clone "$(git config --get remote.origin.url)" "$tmpdir"
make "$tmpdir"
